const parseCsvFile = require("./parse-csv");
const verifyAddress = require("./fetch-data");

// This module contains our main business logic for validating addresses

/**
 * Reads a CSV files with address, city and zip and validates the address
 * If possible it will correct the address, otherwise it will output "Invalid Address"
 * @param { string } filename
 */
async function validateAddressesAndLog(filename) {
  const addresses = parseCsvFile(filename);

  for (const address of addresses) {
    const streetAddress = address["Street Address"];
    const city = address["City"];
    const postalCode = address["Postal Code"];

    const { data } = await verifyAddress(streetAddress, city, postalCode);

    const original = `${streetAddress}, ${city}, ${postalCode}`;
    const formattedAddress = validateAndFormatAddress(data);

    console.log(`${original} -> ${formattedAddress}`);
  }
}

/**
 * Validates and formats an address response from the address validator API
 * If the address is invalid it will return the string "Invalid Address"
 * @param { object } data - address validator response body
 * @returns a formatted string
 */
function validateAndFormatAddress(data) {
  return data.status === "INVALID"
    ? "Invalid Address"
    : `${data.addressline1}, ${data.city}, ${data.postalcode}`;
}

module.exports = {
  validateAddressesAndLog,
  validateAndFormatAddress,
};
