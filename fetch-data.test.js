/**
 * This test just should validate that the correct data is passed into the call.
 * There's no need for us to retest the axios library
 */
const axios = require("axios");
const verifyAddress = require("./fetch-data");

jest.mock("axios");

const mockEndpoint = "https://test-api.com/verify";
const mockApiKey = "test-api-key";
const mockStreetAddress = "123 test ln";
const mockCity = "Dallas";
const mockPostalCode = "75001";

describe("fetch-data tests", () => {
  it("Should verify the address with the correct params", () => {
    process.env.ADDRESS_VALIDATOR_ENDPOINT = mockEndpoint;
    process.env.ADDRESS_VALIDATOR_API_KEY = mockApiKey;
    const mockGet = jest.fn();
    axios.get.mockImplementation(mockGet);

    verifyAddress(mockStreetAddress, mockCity, mockPostalCode);
    expect(mockGet).toHaveBeenCalledWith(mockEndpoint, {
      params: {
        APIKey: mockApiKey,
        CountryCode: "us",
        StreetAddress: mockStreetAddress,
        City: mockCity,
        PostalCode: mockPostalCode,
      },
    });
  });
});
