require("dotenv").config();

const { validateAddressesAndLog } = require("./address");

async function main() {
  const filename = process.argv[2];

  validateArgs(filename);

  try {
    await validateAddressesAndLog(filename);
  } catch (e) {
    console.error("Error processing data", e);
    process.exit(1);
  }
}

function validateArgs(filename) {
  if (!filename) {
    console.error("No csv filename provided");
    process.exit(9);
  } else if (!process.env.ADDRESS_VALIDATOR_ENDPOINT) {
    console.error("No ADDRESS_VALIDATOR_ENDPOINT provided in your .env file");
    process.exit(9);
  } else if (!process.env.ADDRESS_VALIDATOR_API_KEY) {
    console.error("No ADDRESS_VALIDATOR_API_KEY provided in your .env file");
    process.exit(9);
  }
}

main();

module.exports = main;
