/**
 * No need to validate the libaries, let's just make sure the correct methods are called
 */
const fs = require("fs");
const sync = require("./node_modules/csv-parse/dist/cjs/sync.cjs");

const parseCsvFile = require("./parse-csv");

jest.mock("fs");
jest.mock("./node_modules/csv-parse/dist/cjs/sync.cjs");

const mockFileName = "test.csv";
const mockCSVData = `
Street Address, City, Postal Code
123 Test Ln, Omaha, 11111
`;
const mockedResponse = [
  {
    "Street Address": "123 Test Ln",
    City: "Omaha",
    "Postal Code": "11111",
  },
];

describe("parse-csv tests", () => {
  it("should read the file and parse the data", () => {
    const mockRead = jest.fn(() => mockCSVData);
    const mockParse = jest.fn(() => mockedResponse);

    fs.readFileSync.mockImplementation(mockRead);
    sync.parse.mockImplementation(mockParse);

    const data = parseCsvFile(mockFileName);

    expect(mockRead).toHaveBeenCalledWith(mockFileName, "utf8");
    expect(mockParse).toHaveBeenCalledWith(mockCSVData, {
      columns: true,
      ltrim: true,
    });
    expect(data).toEqual(mockedResponse);
  });
});
