const {
  validateAddressesAndLog,
  validateAndFormatAddress,
} = require("./address");

const parseCsvFile = require("./parse-csv");
const verifyAddress = require("./fetch-data");

const testFileName = "test.csv";
const mockData = [
  {
    "Street Address": "123 Test Ln",
    City: "Omaha",
    "Postal Code": "11111",
  },
  {
    "Street Address": "234 Test Dr",
    City: "Boise",
    "Postal Code": "22222",
  },
];
const mockValidResponse = {
  data: {
    status: "VALID",
    addressline1: "123 Test ln",
    city: "city",
    postalcode: "11111-1111",
  },
};

jest.mock("./parse-csv");
jest.mock("./fetch-data");

describe("address tests", () => {
  let mockParse;
  let mockVerify;

  beforeEach(() => {
    mockParse = jest.fn(() => mockData);
    mockVerify = jest.fn(() => Promise.resolve(mockValidResponse));
    parseCsvFile.mockImplementation(mockParse);
    verifyAddress.mockImplementation(mockVerify);
  });

  afterEach(() => {
    mockParse.mockRestore();
    mockVerify.mockRestore();
  });

  it("should format the address when valid", () => {
    const address = validateAndFormatAddress(mockValidResponse.data);
    expect(address).toEqual("123 Test ln, city, 11111-1111");
  });

  it("should return 'Invalid Address' if the status is INVALID", () => {
    const data = {
      status: "INVALID",
    };

    const address = validateAndFormatAddress(data);
    expect(address).toEqual("Invalid Address");
  });

  it("should parse a CSV file", async () => {
    await validateAddressesAndLog(testFileName);
    expect(mockParse).toHaveBeenCalledWith(testFileName);
  });

  it("should verify each address", async () => {
    await validateAddressesAndLog(testFileName);
    expect(mockVerify).toHaveBeenCalledTimes(2);
  });

  it("should log the expected output", async () => {
    const logSpy = jest.spyOn(console, "log");
    await validateAddressesAndLog(testFileName);
    expect(logSpy).toHaveBeenCalledTimes(2);
    expect(logSpy).toHaveBeenNthCalledWith(
      1,
      "123 Test Ln, Omaha, 11111 -> 123 Test ln, city, 11111-1111"
    );
    expect(logSpy).toHaveBeenNthCalledWith(
      2,
      "234 Test Dr, Boise, 22222 -> 123 Test ln, city, 11111-1111"
    );
  });
});
