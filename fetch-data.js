const axios = require("axios");

// This module contains logic for fetching data. If we had additional data calls we would add them here.

/**
 * verifies an address by fetching it from the address validator API
 * https://www.address-validator.net/api.html
 * @param {string} streetAddress
 * @param {string} city
 * @param {string} postalCode
 * @returns {promise} a promise that will return an object with { data } about the address
 */
async function verifyAddress(streetAddress, city, postalCode) {
  return axios.get(process.env.ADDRESS_VALIDATOR_ENDPOINT, {
    params: {
      APIKey: process.env.ADDRESS_VALIDATOR_API_KEY,
      CountryCode: "us",
      StreetAddress: streetAddress,
      City: city,
      PostalCode: postalCode,
    },
  });
}

module.exports = verifyAddress;
