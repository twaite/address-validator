# Address Validator

This is a simple application that uses the [address validator API](https://www.address-validator.net/api.html) to parse addresses and validate that they are correct, or update them if they are formatted incorrectly.

## Setup

This is a node application developed with Node 16. You will need [node](https://nodejs.org/en/) and [npm](https://www.npmjs.com/) installed to get everything running. If you don't have node installed or have a different version of node, I recommend using [nvm](https://github.com/nvm-sh/nvm) to manage multiple versions.

This project was also setup using [yarn](https://yarnpkg.com/). This is not required to run the project but I prefer working with it. To install yarn you can run `npm i -g yarn`.

Prior to running you will need to install dependencies with `yarn`.

In addition to these dependencies, you will also need to setup some local configuration, for this I've used the [dotenv](https://www.npmjs.com/package/dotenv) library. There is an example file (`.env.example`) included in this repo. Just create a `.env` file in this folder and copy the contents of `.env.example` into your `.env` file. After this you should only need to update the API key in order to run the script.

## Usage

This application expects the path to a csv file to be passed to it. The format for this file expects to receive a file containing "Street Address", "City" and "Postal Code". For example:

```
Street Address, City, Postal Code
123 e Maine Street, Columbus, 43215
1 Empora St, Title, 11111
```

Then the script can be invoked by passing the filename: `yarn start example.csv`.

## Tests

Run the tests with `yarn test`

## Enhancements / Notes

- In a professional JS project I would always recommend using TypeScript, for the sake of time and simplicity (since it would require transpiling) I left this out and instead focused on jsdoc comments.
- If we were worried about performance we could make the requests in parallel.
- This code is broken up into modules more than strictly necessary to demonstrate a way you may want to organize your calls. For example, the data call is split into it's own module from the logic about the addresses themselves.
- I would like to have added some tests to the index file to make sure the validation for the inputs was correct, but I ran out of time.
