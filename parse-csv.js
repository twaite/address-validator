const fs = require("fs");
/**
 * NOTE: tw - this is not an ideal import but is a problem in this dep right now
 * see: https://github.com/adaltas/node-csv/issues/323
 */
const { parse } = require("./node_modules/csv-parse/dist/cjs/sync.cjs");

// This module contains logic for parsing CSV files

/**
 * Parses a CSV file into an object given a filename
 * @param { string } filename
 * @returns { array } CSV data parsed into an array of objects
 */
function parseCsvFile(filename) {
  const data = fs.readFileSync(filename, "utf8");

  return parse(data, {
    columns: true,
    ltrim: true,
  });
}

module.exports = parseCsvFile;
